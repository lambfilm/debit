<?php

namespace App\Model;

use App\Core\{
    AbstractModel, EntityInterface
};
use PDO;

class Balance extends AbstractModel implements EntityInterface
{
    const OUT_OF_RANGE_TEXT_ERROR = 'Your amount out of range';
    const OTHER_ERROR = 'Something went wrong';

    const CURRENCY = 2;
    const DECIMAL_POINT = '.';

    public function findBalanceByUser (int $userId)
    {
        $sql = "SELECT `amount` FROM balance WHERE user_id = '$userId'";

        $result = $this->db->query($sql)->fetch(PDO::FETCH_ASSOC);

        if ($result) {
            return $result['amount'];
        }
        return false;
    }
}
