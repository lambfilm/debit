<?php

namespace App\Model;

use App\Core\{AbstractModel, Database, EntityInterface};
use PDO;

class User extends AbstractModel implements EntityInterface
{
    protected int $id;
    protected float $balance;
    protected string $login;
    protected string $hash;

    public function setId(int $id): EntityInterface
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setLogin(string $login): EntityInterface
    {
        $this->login = $login;

        return $this;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setHash(string $hash): EntityInterface
    {
        $this->hash = $hash;

        return $this;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function updateBalance (int $userId, string $amount)
    {
        $sql = "            
            SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
                SELECT `amount` INTO @balance FROM balance WHERE `user_id` = '$userId';
                UPDATE balance SET balance.amount = (@balance - '$amount') WHERE user_id = '$userId';
            COMMIT;  
        ";
        $this->db->exec($sql);

        return $this->db->errorCode();
    }

    public function findByLogin (string $login)
    {
        $sql = "SELECT id, login, hash FROM users WHERE login = '$login'";

        $result = $this->db
            ->query($sql)
            ->fetch(PDO::FETCH_ASSOC)
        ;
        if ($result) {
            $this
                ->setId($result['id'])
                ->setLogin($result['login'])
                ->setHash($result['hash'])
            ;
            return $this;
        }
        return false;
    }

    public function verify (string $password): bool
    {
        $logoPass = $this->getLogin() . $password;

        if(password_verify($logoPass, $this->getHash())) {
            return true;
        }
        return false;
    }
}
