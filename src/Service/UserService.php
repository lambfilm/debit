<?php

namespace App\Service;

use App\Core\Database;
use App\Dto\UserDto;
use App\Model\User;
use Exception;

class UserService
{
    protected User $user;
    protected string $amount;

    public function __construct(UserDto $userDto)
    {
        $this->user = $userDto->user;
        $this->amount = $userDto->amount;
    }

    public function flashBalance (): bool
    {
        $errorCode = $this->user->updateBalance($this->user->getId(), $this->amount);

        if ($errorCode !== Database::NO_SQL_ERRORS) {
            throw new Exception(Database::getErrorTextFromCode($errorCode));
        }
        return true;
    }
}
