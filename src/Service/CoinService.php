<?php

namespace App\Service;

/**
 * @deprecated 
 */
class CoinService
{
    const CONVERT_UNIT = 100;

    const CONVERT_DOWN = 'down';
    const CONVERT_UP = 'up';

    public static function convertDown (float $coin): ?float
    {
        if ($coin > 0) {
            return ($coin / self::CONVERT_UNIT);
        }
        return false;
    }

    public static function convertUp (float $coin): ?float
    {
        return ($coin * self::CONVERT_UNIT);
    }

    public static function calculate (float $balance, float $amount): ?float
    {
        if ($balance > 0) {
            $calculate = $balance - ($amount * self::CONVERT_UNIT);

            return ($calculate > 0) ?? $calculate;
        }
        return false;
    }
}
