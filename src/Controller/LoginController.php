<?php

namespace App\Controller;

use App\Core\AbstractController;
use App\Core\Request;
use App\Model\User;

class LoginController extends AbstractController
{
    /**
     * @return void
     */
    public function index ()
    {
        $this->render('auth/login.php');
    }

    /**
     * @return void
     */
    public function login ()
    {
        if (isset($this->request->post)
            && !empty($this->request->post)
            && Request::METHOD_POST === $this->request->method
        ) {
            $login = $this->request->getPostValueFromKey('login');
            $password = $this->request->getPostValueFromKey('password');

            $user = (new User)->findByLogin($login);

            if ($user && $user->verify($password)) {
                $this->session->userId = $user->getId();
                $this->response->redirectToRoute('balance');
            }
        }
        $this->render('auth/login.php');
    }
}
