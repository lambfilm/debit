<?php

namespace App\Controller;

use App\Dto\UserDto;
use App\Model\{
    Balance, User
};
use App\Core\{
    AbstractController, Request
};
use App\Service\UserService;
use Exception;

class BalanceController extends AbstractController
{
    public function index ()
    {
        $balance = (new Balance())
            ->findBalanceByUser($this->session->userId);

        return $this->render('balance/index.php', [
            'balance' => $balance,
        ]);
    }

    /**
     * @throws Exception
     */
    public function update ()
    {
        if (isset($this->request->post)
            && !empty($this->request->post)
            && Request::METHOD_POST === $this->request->method
        ) {
            $userDto = UserDto::fromArray([
                'user'   => new User($this->session->userId),
                'amount' => $this->request->getPostValueFromKey('amount')
            ]);
            $service = new UserService($userDto);

            if ($service->flashBalance()) {
                $this->response->redirectToRoute('balance');
            }
        }
    }
}
