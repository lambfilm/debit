<?php

namespace App\Dto;

use App\Model\Balance;
use App\Model\User;

class UserDto
{
    private static ?UserDto $dto = null;
    public User $user;
    public ?string $amount = null;

    public static function fromArray(array $data): UserDto
    {
        if (null === self::$dto) {
            self::$dto = new self();
        }
        if (key_exists('user', $data)) {
            self::$dto->user = $data['user'];
        }
        if (key_exists('amount', $data)) {
            self::$dto->amount = self::prepareAmount($data['amount']);
        }
        return self::$dto;
    }

    public static function prepareAmount(string $amount): string
    {
        if (false !== strpos($amount, ',')) {
            $amount = str_replace(',', '.', $amount);
        }
        $amount = (float) $amount;

        return number_format($amount, Balance::CURRENCY, Balance::DECIMAL_POINT, ' ');
    }
}
