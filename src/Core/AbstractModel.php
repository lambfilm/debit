<?php

namespace App\Core;

use PDO;

abstract class AbstractModel implements EntityInterface
{
    protected PDO $db;

    public function __construct($entity = null)
    {
        $this->db = Database::getInstance()->getConnection();

        if (isset($entity)) {
            $entityId = (is_int($entity)) ? $entity : $entity->getId();

            $this->find($entityId);
        }
    }

    /**
     * TODO пока только для юзера
     */
    public function find (int $userId): EntityInterface
    {
        $sql = "SELECT `id`, `login`, `hash`, `balance` FROM users WHERE id = {$userId}";

        $result = $this->db
            ->query($sql)
            ->fetch(PDO::FETCH_ASSOC)
        ;
        if ($result) {
            $this->setId($result['id']);
            $this->setLogin($result['login']);
            $this->setHash($result['hash']);
        }
        return $this;
    }
}
