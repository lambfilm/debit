<?php

namespace App\Core;

class Request implements RequestInterface
{
    const METHOD_GET = 'GET';
    const METHOD_POST= 'POST';

    const LOGIN_PAGE = 'login';

    public string $uri;
    public string $method;
    public ?array $post;

    public function __construct()
    {
        $this->setUri();
        $this->setMethod();
        $this->setPostParams();
    }

    public function getPostValueFromKey (string $key): ?string
    {
        if (key_exists($key, $this->post)) {
            return trim(strip_tags($this->post[$key]));
        }
        return false;
    }

    public function setPostParams(): Request
    {
        if (isset($_POST)) {
            $this->post = $_POST;
        }
        return $this;
    }

    public function setUri(): Request
    {
        $this->uri = current(explode('/', trim($_SERVER['REQUEST_URI'], '/')));;

        return $this;
    }

    public function setMethod(): Request
    {
        $this->method = $_SERVER['REQUEST_METHOD'];

        return $this;
    }
}
