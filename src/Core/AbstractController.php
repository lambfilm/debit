<?php

namespace App\Core;

abstract class AbstractController
{
    protected Request $request;
    protected Response $response;
    protected Session $session;

    public function __construct()
    {
        $this->request  = new Request();
        $this->response = new Response();
        $this->session  = Session::getInstance();
    }

    protected function render(string $template, array $data = [])
    {
        $file = __DIR__ . '/../View/' . $template;

        if (file_exists($file)) {
            require_once $file;
        }else{
            die("Template {$file} not found");
        }
        return $this;
    }
}
