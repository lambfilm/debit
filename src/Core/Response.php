<?php

namespace App\Core;

class Response
{
    public function redirectToRoute (string $route = null)
    {
        $path = $route ?? getenv('HOME_PAGE');

        header("Location: $path");
    }
}
