<?php

namespace App\Core;

use App\Controller\{
    BalanceController, LoginController
};

class Route
{
    public string $controller;
    public string $action;
    public array $params = [];
    public Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public static function get(): array
    {
        return [
            'balance' => [BalanceController::class, 'index'],
            'login'   => [LoginController::class, 'index'],
        ];
    }
    
    public static function post(): array
    {
        return [
            'balance' => [BalanceController::class, 'update'],
            'login'   => [LoginController::class, 'login'],
        ];
    }

    public static function getRoutes(Request $request): ?array
    {
        switch ($request->method) {
            case Request::METHOD_GET : $routes = Route::get(); break;
            case Request::METHOD_POST: $routes = Route::post(); break;
            default : $routes = null;
        }
        return $routes;
    }

    public function fromRequest()
    {
        if ($this->request->uri && $this->request->method) {
            if (array_key_exists($this->request->uri, $routes = $this->getRoutes($this->request))) {
                $this->controller = current($routes[$this->request->uri]);
                $this->action = end($routes[$this->request->uri]);

                return $this;
            }
        }
        return false;
    }
}
