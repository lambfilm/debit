<?php

namespace App\Core;

use App\Model\Balance;
use PDO;

class Database
{
    const OUT_OF_RANGE_ERROR = '22003';
    const NO_SQL_ERRORS = '00000';

    private static ?Database $_instance = null;
    private ?PDO $_connect = null;

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

    public static function getInstance(): Database
    {
        if (null === self::$_instance) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    public function getConnection(): PDO
    {
        if (!self::$_instance->_connect) {
            $host = getenv('MYSQL_HOST');
            $user = getenv('MYSQL_ROOT_USER');
            $pass = getenv('MYSQL_ROOT_PASSWD');
            $name = getenv('MYSQL_DBNAME');

            $host = '127.0.0.1';
            $user = 'startup';
            $pass = 'DcTG24YK';
            $name = 'startup';

            self::$_instance->_connect = new PDO("mysql:host=$host;dbname=$name", $user, $pass);
        }
        return self::$_instance->_connect;
    }

    public static function getErrorTextFromCode (string $code): string
    {
        switch ($code) {
            case Database::OUT_OF_RANGE_ERROR:
                $text = Balance::OUT_OF_RANGE_TEXT_ERROR; break;
            default:
                $text = Balance::OTHER_ERROR;
        }
        return $text;
    }
}
