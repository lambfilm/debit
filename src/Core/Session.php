<?php

namespace App\Core;

class Session
{
    private ?bool $_state = null;
    private static ?Session $_instance = null;

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

    public static function getInstance(): Session
    {
        if (null === self::$_instance) {
            self::$_instance = new self;
        }
        self::$_instance->startSession();

        return self::$_instance;
    }

    public function startSession(): bool
    {
        if (!self::$_instance->_state) {
            $this->_state = session_start();
        }
        return $this->_state;
    }

    public function __set(string $key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function __get(string $key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
        return false;
    }

    public function __isset(string $key): bool
    {
        return isset($_SESSION[$key]);
    }

    public function __unset(string $key): void
    {
        unset($_SESSION[$key]);
    }
}
