<?php

namespace App;

use App\Core\AbstractController;
use App\Core\Request;
use App\Core\Route;

class App extends AbstractController
{
    public function start()
    {
        if (!$this->session->userId && Request::LOGIN_PAGE !== $this->request->uri) {
            $this->response->redirectToRoute(Request::LOGIN_PAGE);
        }

        $route = (new Route($this->request))->fromRequest();

        if (false === $route) {
            die('404 Page not found');
        }
        call_user_func_array([new $route->controller, $route->action], $route->params);
    }
}
