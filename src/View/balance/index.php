<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DOWN PAYMENT</title>
    <style> *{font-family: Tahoma, Arial, sans-serif;}</style>
</head>
<body>
    <?php
        echo "<h1>YOUR DEBIT: {$data['balance']} ₽</h1>";
    ?>
    <form action="/balance" method="post">
        <label for="user_amount">
            <input
                type="number"
                step="0.01"
                id="user_amount"
                name="amount"
                placeholder="Insert amount here..."
            />
        </label>
        <button type="submit">ВЫВЕСТИ</button>
    </form>
</body>
</html>
