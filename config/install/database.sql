CREATE DATABASE IF NOT EXISTS debit;

USE debit;

DROP TABLE IF EXISTS `users`;

-- users
CREATE TABLE `users` (
    `id`       INT PRIMARY KEY AUTO_INCREMENT,
    `login`    VARCHAR (255) NOT NULL,
    `hash`     VARCHAR (255) NOT NULL,
    `balance`  BIGINT DEFAULT 0 NOT NULL,
    UNIQUE KEY `login` (`login`,`hash`),
    UNIQUE KEY `UQ_login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- balance
CREATE TABLE IF NOT EXISTS `balance` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
    `amount` DECIMAL(12,2) unsigned NOT NULL DEFAULT '0.00',
    `user_id` INT unsigned DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Password for user: 1234
INSERT INTO users (`login`, `hash`, `balance`) VALUES('hashBon', '$2y$10$3RaNq7jtmKTgTzfEBFc2Hud8kfnQ4KS8XYedd60A7/jym81VrpElC', 100000);
INSERT INTO balance (`user_id`, `amount`) VALUES(1, '1000.00');