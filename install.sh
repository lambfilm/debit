#!/bin/bash
ACTION=$1
declare -a files

function create_structure_db()
{
    sleep 2;
    echo -en "\033[41m\033[30m DEFAULT VALUE: 'secret'\033[0m"
    echo -e "\r"
    source .env
    docker-compose exec mysql /bin/bash -c 'mysql -u root -p'$MYSQL_ROOT_PASSWORD' < "/home/database.sql"'
}

function start_containers()
{
    echo -en "\033[41m\033[30m START CONTAINERS \033[0m"
    echo -e "\r"
    docker-compose -f ./docker-compose.yml down
    sleep 2;
    docker-compose -f ./docker-compose.yml up -d
}

function composer_install_and_update() {
    sleep 1;
    echo -e "\033[41m\033[30m COMPOSER INSTALL \033[0m"
    /bin/bash -c "composer install --ignore-platform-reqs --no-interaction"
    echo -e "\033[41m\033[30m COMPOSER DUMP-AUTOLOAD \033[0m\r"
    sleep 3;
    /bin/bash -c "composer dump-autoload"
}

function print_info_after_install() {
    sleep 1;
    echo -en "\r"
    echo -e "\033[41m\033[30m============================================\033[0m"
    echo -e "\033[41m\033[30m  LINK TO USE: http://debit.local/balance   \033[0m"
    echo -e "\033[41m\033[30m============================================\033[0m"
}

start_containers
composer_install_and_update
create_structure_db
print_info_after_install
